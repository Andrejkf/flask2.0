FROM python:3.8

ENV APPPATH /public
COPY . $APPPATH
WORKDIR $APPPATH/app

RUN pwd \
&& ls -lah \
&& pip install -r requirements.txt \
&& pwd \
&& ls -lah

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["app1.py"]

