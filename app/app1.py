from flask import Flask

# 1. Instantiate a class
app = Flask(__name__) #Create a Flask application instance with the name "app"

# 2. Path Web requests/responses
@app.route('/')
def displayMessage():
    return "Hello. Today is Monday. May 17, 2021."


if __name__=='__main__':
    app.run(debug=True, host='0.0.0.0')