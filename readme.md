##### Flask Hello World in Docker at localhost

After reading [this](https://codebabel.com/ci-gitlabci-docker/)

1. Create folder structure. (Project layout)

    See this both documentation for project layout:

* [Large Applications](https://flask.palletsprojects.com/en/1.1.x/patterns/packages/)
* [How to structure large Flask Applications](https://www.digitalocean.com/community/tutorials/how-to-structure-large-flask-applications)

2. Create `app1.py`

```python
from flask import Flask
from flask_frozen import Freezer

# 1. Instantiate a class
app = Flask(__name__) #Create a Flask application instance with the name "app"
freezer = Freezer(app) # Create a Freezer Instance with the name "freezer"

# 2. Define Paths for Web pages using flask-frozen
app.config['FREEZER_BASE_URL'] = 'https://andrejkf.gitlab.io/' # Set up URL
app.config['FREEZER_DESTINATION'] = 'public' # Set folder to paste files

# 3. Define Custom functions
@app.cli.command("renderCustomCommand") # To define custom command "renderCustomCommand" . Command Line Interface in a composable way with as little code as necessary
def renderCustomCommand(): #custom command definiton to be able to upload files to public folder in the Website
    freezer.freeze()

# 4. Path Web requests/responses
@app.route('/')
def displayMessage():
    return "Hello. Today is Monday. May 17, 2021."
```

3. Create  `Dockerfile`

```dockerfile
FROM python:3.8
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt \
&& pwd \
&& ls -lah

EXPOSE 5000

ENTRYPOINT [ "python" ]
CMD [ "app1.py" ]
```



4. Created `requirements.txt`

```
flask
frozen-flask
```

5. Created `.gitlab-ci.yml` file:

```bash
stages:
    - build

docker-build:
  image: docker:latest
  
  stage: build
  
  services:
    - docker:dind
  
  before_script:
    -  echo $CI_BUILD_TOKEN | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
```

6. Created git repository

```bash
git init
git status
git add .
git commit -m "First commit"
git push --set-upstream git@gitlab.com:Andrejkf/flask2.git master
```

​	6.1 Change "private" repository to "public".

​	Settings -> General -> Visibility, project features, permissions -> Project Visibility 

​	And save changes.

7. If pipeline went right then :

    Packages and registries -> Container Registry

    Then copy that container registry:

    `registry.gitlab.com/andrejkf/flask2.0`

8. Then docker pull from GitLab registry:

```bash
docker pull registry.gitlab.com/andrejkf/flask2.0
```

9. Then check docker images and containers:

```bash
docker images
docker ps -a
docker ps
```



10. Then create container:

```bash
docker run -d -p 5000:5000 registry.gitlab.com/andrejkf/flask2.0
```

or you can use:

```bash
docker run -it -p 5000:5000 registry.gitlab.com/andrejkf/flask2.0
```



11. Then go to `localhost:5000` and test it out.